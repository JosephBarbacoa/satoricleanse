"use strict";
/* jshint node: true */
module.exports = function(grunt) {
    var path = require("path"),
        banner;

    banner = "/*! <%= pkg.name %> - v <%= pkg.version %> - " +
        "<%= grunt.template.today('yyyy-mm-dd') %>\n" +
        "<%= pkg.homepage ? '* ' + pkg.homepage + '\\n' : '' %>" +
        "* Copyright (c) <%= grunt.template.today('yyyy') %> <%= pkg.author.name %>;" +
        " Licensed <%= _.pluck(pkg.licenses, 'type').join(', ') %> */\n";

    require("time-grunt")(grunt);
    //Load our custom tasks
    grunt.loadTasks("build-utils/grunt/tasks");

    require("load-grunt-config")(grunt, {
        configPath: path.join(process.cwd(), "build-utils/grunt/config"), //path to task.js files, defaults to grunt dir
        init: true, //auto grunt.initConfig
        data: { //data passed into config.
            banner: banner,
            pkg: grunt.file.readJSON("package.json"),
            layerName: "satoricleansing",
            srcDir: "src/satoricleansing",
            buildDistDir: "dist/<%= layerName %>",
            buildUtilsDir: "build-utils",
            releaseDir: "release",
            testDir: "test",
            devServerPort: 32023
        },
        loadGruntTasks: { //can optionally pass options to load-grunt-tasks.  If you set to false, it will disable auto loading tasks.
            pattern:  ["grunt-*", "intern"]
        }
    });

    // always run to copy updated git hooks if any.
    grunt.task.run(["exec:installdepsbower"]);

    //Linting tasks
    grunt.registerTask("lint", ["jshint"]);

    //Minifier
    grunt.registerTask("minify", ["uglify"]);

    grunt.registerTask("bundle", ["requirejs", "minify"]);

    grunt.registerTask("build", ["clean:build", "lint", "bundle", "sass", "copy:config", "rename:config"]);

    grunt.registerTask("dev", ["compass", "watch"]);

    // Start a node server to serve plugin files
    grunt.registerTask("devServer", "Start a node server to serve files from src", ["compass:compile", "exec:appDev"]);
    grunt.registerTask("releaseServer", "Start a node server to serve files from dist", ["exec:appRelease"]);

    // Default task.
    grunt.registerTask("default", "build");
};
