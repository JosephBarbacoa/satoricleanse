/* jshint node: true */
/**
 * This modules provides the grunt task for running unit tests for morpheus.
 * @module tasks/test
 */

/**
 * Register the test task to grunt.
 *
 * This task can be executed using 'grunt test'. Some options can also be provided on the command line.
 * - suites: A comma separted list of suite files to run. e.g. 'grunt test --suites=test/unit/baseObjectIncludeTest'
 * - no-headless: To run the tests in the browser during development for debugging. e.g. 'grunt test --no-headless'.
 *
 * @type GruntTask
 * @param {Object} grunt - the grunt context
 */
module.exports = function(grunt) {

    grunt.registerTask("test", "Run unit tests", function() {
        var debugMode = grunt.option("headless") === false ? true : false,
            suites = [],
            path = require("path"),
            jsExtensionRegex = /\.js$/,
            done,
            testUrl,
            proxy;

        if (grunt.option("suites")) {
            suites = grunt.option("suites").split(",");
            suites = suites.map(function(suite) {

                // strip out the js extension if provided in the suite name. Suite names should be module ids
                // and not js files. The loader interprets the two differently.
                return suite.replace(jsExtensionRegex, "");
            });
        }

        if (!debugMode) {
            done = this.async();
            if (suites.length > 0) {
                grunt.config(["intern", "runner", "options", "suites"], suites);
            }

            proxy = grunt.util.spawn({
                cmd: "java",
                args: ["-jar", path.normalize("node_modules/selenium-server-standalone-jar/jar/selenium-server-standalone-2.43.1.jar")]
            }, function(err) {
                if (err) {
                    grunt.log.error(err.message);
                }
            });

            setTimeout(function() {
                grunt.log.writeln("Proxy Started:" + proxy.pid);
                grunt.task.run(["intern:runner", "copy:testreports", "clean:interndefaultreports"]);
                done();
            }, 1000);

            grunt.file.copy(path.normalize("tests/runner/patch/client.html"), path.normalize("node_modules/intern/client.html"));
            process.on("exit", function() {
                proxy.kill();
            });

        } else {
            testUrl = "http://localhost:" + grunt.config("devServerPort") + "/node_modules/intern/client.html?config=tests/runner/config/intern&reporters=html";
            console.log(testUrl);
            suites.forEach(function(suite) {
                testUrl += "&suites=" + suite;
            });

            grunt.config("test.testUrl", testUrl);
            grunt.task.run(["connect:test"]);
        }

    });

};
