/* jshint node: true */
/**
 * This modules provides the grunt task for creating component releases. The task will increment version numbers in
 * package.json and bower.json, create release files with the correct version, creates a commit with
 * the new release files, package.json and bower.json file. It also creates a git tag.
 * @module tasks/release
 */

/**
 * Register the release task to grunt.
 *
 * This task can be executed using 'grunt release'. Some options can also be provided on the command line.
 * - prerelease: to create a prerelease release, e.g. grunt release --prerelease.
 * - patch: to create a patch release, e.g. grunt release --patch.
 * - minor: to create a minor release, e.g. grunt release --minor.
 * - major: to create a major release, e.g grunt release --major.
 * - setversion: to set a version explicitly, e.g. grunt release --setversion=0.1.4-beta.
 *
 * If no argument is provided, a minor release will be created.
 *
 * @type GruntTask
 * @param {Object} grunt - the grunt context
 */
module.exports = function(grunt) {

    grunt.registerTask("release", "Create release", function() {
        var explicitVersion = grunt.option("setversion"),
            bumpOnlyTask = "bump-only",
            predefinedVersions = ["major", "minor", "patch", "prerelease"],
            i, predefinedVersion;

        if (!explicitVersion) {
            for (i = 0; i < predefinedVersions.length; i++) {
                predefinedVersion = predefinedVersions[i];
                if (grunt.option(predefinedVersion)) {
                    bumpOnlyTask += ":" + predefinedVersion;
                    break;
                }
            }
        }

        grunt.task.run([bumpOnlyTask, "build", "copy:release", "bump-commit"]);

    });

};