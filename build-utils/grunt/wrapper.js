/**
 * Dummy loader for replacing loaders that we don't link
 */
define(function () {
    return {load: function (id, parentRequire, loaded) {
        loaded();
    }};
});