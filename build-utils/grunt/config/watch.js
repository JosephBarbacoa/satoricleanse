/* jshint node: true */

module.exports = {
    sass: {
        files: ["<%= srcDir %>/sass/**/*.scss"],
        tasks: ["compass:compile"],
        options: {
            // This is necessary as it allows us to change the config of compass compile in the
            // watch event handler to compile only changed files.
            nospawn: true
        }
    }
};