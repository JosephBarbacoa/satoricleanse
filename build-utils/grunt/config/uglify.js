/* jshint node: true */

module.exports = {
    options: {
        banner: "<%= banner %>"
    },
    dist: {
        options: {
            sourceMap: true
        },
        src: "<%= buildDistDir %>/<%= layerName %>.js.uncompressed.js",
        dest: "<%= buildDistDir %>/<%= layerName %>.js",
        sourceMap: true
    }
};
