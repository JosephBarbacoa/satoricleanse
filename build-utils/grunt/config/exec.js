/* jshint node: true */

module.exports = {
    installdepsbower: {
        command: "bower install",
        callback: function (err) {
            if (err) {
                grunt.log.writeln("Error running bower install. Try manually.");
            }
        }
    },
    appDev: {
        command: "node app.js"
    },
    appRelease: {
        command: "node app.js dist"
    }
};
