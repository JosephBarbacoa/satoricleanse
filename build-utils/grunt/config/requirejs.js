/* jshint node: true */

module.exports = {
    compile: {
        options: {
            baseUrl: "./",
            preserveLicenseComments: false,
            optimize: "none",
            paths: {
                "dijit": "empty:",
                "dojo": "empty:",
                "dojo/has": "build-utils/grunt/wrapper",
                "dojo/i18n": "build-utils/grunt/wrapper",
                "dojox": "empty:",
                "ecosystem-app": "empty:",
                "neo": "empty:",
                "text": "bower_components/requirejs-text/text",
                "satoricleansing": "<%= srcDir %>",
                "jquery": "bower_components/jquery/dist",
                "xstyle/css": "build-utils/grunt/wrapper",
                // ToDo: refactor the following
                "qbo/util/track": "empty:",
                "qbo/util/plugins": "empty:",
                "morpheus": "empty:"
            },
            normalizeDirDefines: "all",
            out: "<%= buildDistDir %>/<%= layerName %>.js.uncompressed.js",
            exclude: [
                "dojo/has",
                "dojo/i18n",
                "text"
            ],
            include: [
                "<%= pkg.name %>/SingleAddressCleansePluginViewController",
                "dojo/i18n!<%= pkg.name %>/nls/Starter"
            ]
        }
    }
};
