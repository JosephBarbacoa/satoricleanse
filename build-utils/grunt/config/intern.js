/* jshint node: true */

module.exports = {
    runner: {
        options: {
            config: "tests/runner/config/intern",
            runType: "runner",
            reporters: ["console", "lcovhtml", "lcov", "cobertura", "junit"]
        }
    }
};
