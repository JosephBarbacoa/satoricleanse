/* jshint node: true */

module.exports = {
    dist: {
        options: {
            outputStyle: "compressed"
        },
        files: {
            "<%= buildDistDir %>/styles/Plugin.css": ["<%= srcDir %>/sass/Plugin.scss"]
        }
    }
};
