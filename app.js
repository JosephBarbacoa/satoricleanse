var port = 34212;
var localServerName = "localhost.intuit.com:" + port;

var express = require("express");
var morgan = require("morgan");
var errorHandler = require("errorhandler");
var https = require("https");
var path = require("path");
var fs = require("fs");
var cors = require("cors");

var app = express();
var rootDir;

// Get root dir
if (process.argv.length === 3) {
    rootDir = "/" + process.argv[2];
} else {
    rootDir = "/src";
}

// all environments
app.set("port", process.env.PORT || port);
app.use(morgan("combined"));
app.use(cors());
app.options("*", cors());
app.use("/", express.static(__dirname + rootDir));

if ("development" === app.get("env")) {
    app.use(errorHandler());
}

var privateKey = fs.readFileSync("sslcert/server.key");
var certificate = fs.readFileSync("sslcert/server.crt");

var options = {key: privateKey, cert: certificate};

https.createServer(options, app).listen(app.get("port"), function () {
    console.log("Serving files from:", rootDir);
    console.log("Visit https://localhost.intuit.com:" + app.get("port"));
});
