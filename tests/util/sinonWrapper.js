/*jshint undef:false*/
define([
    "qbotest/libs/sinon.js"
], function () {
    /**
     * Provides a wrapper around the sinon global object so our test code does not have
     * to use the global object but can require this.
     */
    return sinon;
});