var page = require("webpage").create(),
    logger = require("../utils/consoleLogger"),
    utils = require("../utils/utils"),
	timeout = 130000,
	args = null,
	url = null,
	reporter = null,
    onSuiteEnd = null,
    exitPhantom = null;

args = require("system").args;
if (args.length === 1) {
    logger.writeln("Pass in a test url as the second parameter");
    exitPhantom(true);
}

setTimeout(function () {
	logger.error("Timed out. Test run did not complete.");
	exitPhantom(true);
}, timeout);

/*jshint undef:false */
url = args[1];

onSuiteEnd = function (suite) {
    var error;
    if (utils.isMainSuite(suite)) {
        error = suite.numFailedTests ? true : false;
        exitPhantom(error);
    }
};

exitPhantom = function (error) {
    if (error) {
        phantom.exit(1);
    } else {
        phantom.exit();
    }
};

reporter = require("../reporters/commonConsoleReporter");

page.onCallback = function (event) {
	var topic = event.topic;

	if (typeof reporter[topic] === "function") {
		reporter[topic](event.data);
	}
    if (topic === "/suite/end") {
        onSuiteEnd(event.data);
    }
};

page.onResourceError = function (resourceError) {
    logger.error("Unable to load resource (#" + resourceError.id + "URL:" + resourceError.url + ")");
    logger.error("Error code: " + resourceError.errorCode + ". Description: " + resourceError.errorString);
    exitPhantom(true);
};

logger.writeln("Loading a web page");
logger.writeln(url);
page.open(url, function (status) {
	logger.writeln(status);
});

