// Learn more about configuring this file at <https://github.com/theintern/intern/wiki/Configuring-Intern>.
// These default settings work OK for most people. The options that *must* be changed below are the
// packages, suites, excludeInstrumentation, and (if you want functional tests) functionalSuites.
define([], function () {

    var plugin = "satoricleansing";

    //TODO: find a better way to fix global object dependencies. This is needed here as
    //some modules e.g. amount will not even load without the qbo object and the given
    //properties.
    /*jshint -W020 */
// TODO Get rid of all QBO dependencies
    qbo = {
        companyL10nAttribs: {},
        ipd: {recommendations: {square: {enabled: false}}},
        menus: [],
        sku: {},
        sitecatalyst: {enabled: false}
    };

    return {
        // The port on which the instrumenting proxy will listen
        proxyPort: 9000,

        // A fully qualified URL to the Intern proxy
        proxyUrl: "http://localhost:9000/",

        // Default desired capabilities for all environments. Individual capabilities can be overridden by any of the
        // specified browser environments in the `environments` array below as well. See
        // https://code.google.com/p/selenium/wiki/DesiredCapabilities for standard Selenium capabilities and
        // https://saucelabs.com/docs/additional-config#desired-capabilities for Sauce Labs capabilities.
        // Note that the `build` capability will be filled in with the current commit ID from the Travis CI environment
        // automatically
        capabilities: {
            "selenium-version": "2.37.0"
        },

        // Browsers to run integration testing against. Note that version numbers must be strings if used with Sauce
        // OnDemand. Options that will be permutated are browserName, version, platform, and platformVersion; any other
        // capabilities options specified for an environment will be copied as-is
        environments: [ { browserName: "phantom" } ],

        // Maximum number of simultaneous integration tests that should be executed on the remote WebDriver service
        maxConcurrency: 3,

        // Whether or not to start Sauce Connect before running tests
        useSauceConnect: false,

        // Connection information for the remote WebDriver service. If using Sauce Labs, keep your username and password
        // in the SAUCE_USERNAME and SAUCE_ACCESS_KEY environment variables unless you are sure you will NEVER be
        // publishing this configuration file somewhere
        webdriver: {
            host: "localhost",
            port: 4444
        },

        // Configuration options for the module loader; any AMD configuration options supported by the Dojo loader can be
        // used here
        loader: {
            // Packages that should be registered with the loader in each testing environment
            packages: [
                // QBO-UI and dependencies
                { name: "neo", location: "bower_components/qbo-ui-libs-neo/src" },
                { name: "dojo", location: "bower_components/dojo" },
                { name: "dijit", location: "bower_components/dijit" },
                { name: "dojox", location: "bower_components/dojox" },
                { name: "dgrid", location: "bower_components/dgrid" },
                { name: "xstyle", location: "bower_components/xstyle" },
                { name: "text", location: "bower_components/requirejs-text", main: "text" },

                // QBO-UI Bower components
                {
                    name: "backbone",
                    main: "backbone",
                    location: "bower_components/backbone"
                },
                {
                    name: "morpheus",
                    location: "bower_components/morpheusjs/src"
                },
                {
                    name: "qbo-ui-libs-math",
                    location: "bower_components/qbo-ui-libs-math/src"
                },
                {
                    name: "qbo-ui-libs-xdm",
                    location: "bower_components/qbo-ui-libs-xdm/src"
                },
                {
                    name: "ecosystem-app",
                    main: "ecosystem-app",
                    location: "bower_components/ecosystem-app/src/js"
                },
                {
                    name: "underscore",
                    main: "underscore",
                    location: "bower_components/underscore"
                },
                {
                    name: "jquery",
                    main: "jquery",
                    location: "bower_components/jquery/dist"
                },
                { name: "dcl", location: "bower_components/dcl" },
                { name: "rsvp", location: "bower_components/rsvp" },

                // Plugin
                { name: plugin, location: "src/" + plugin }
            ]
        },

        // Non-functional test suite(s) to run in each browser
        suites: [ "tests/all" ],

        // Functional test suite(s) to run in each browser once non-functional tests are completed
        functionalSuites: [ /* "myPackage/tests/functional" */ ],

        // A regular expression matching URLs to files that should not be included in code coverage analysis
        excludeInstrumentation: /^(?:dojo|buildTmp|node_modules|tests|libs|qbo\/nls|dojo-patch)\//
    };
});
