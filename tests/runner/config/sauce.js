// Learn more about configuring this file at <https://github.com/theintern/intern/wiki/Configuring-Intern>.
// These default settings work OK for most people. The options that *must* be changed below are the
// packages, suites, excludeInstrumentation, and (if you want functional tests) functionalSuites.
define([
    "node_modules/intern/node_modules/dojo/lang",
    "./base"
], function (lang, base) {

    var config = {

        // Whether or not to start Sauce Connect before running tests.
        useSauceConnect: true,

        environments: [
            { browserName: "chrome", platform: [ "OS X 10.8", "OS X 10.9", "Windows 7", "Windows 8" ] },
            { browserName: "firefox", version: "27", platform: [ "Windows 7", "Windows 8" ] }
        ],
        // Non-functional test suite(s) to run in each browser
        suites: [ "tests/all" ],

        // Functional test suite(s) to run in each browser once non-functional tests are completed
        functionalSuites: [ /* "myPackage/tests/functional" */ ],

        // A regular expression matching URLs to files that should not be included in code coverage analysis
        excludeInstrumentation: /^(?:dojo|tests|libs|qbo\/nls|dojo-patch)\//
    };

    config = lang.mixin(base, config);
    return config;
});