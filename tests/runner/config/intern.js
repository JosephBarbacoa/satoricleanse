// Learn more about configuring this file at <https://github.com/theintern/intern/wiki/Configuring-Intern>.
// These default settings work OK for most people. The options that *must* be changed below are the
// packages, suites, excludeInstrumentation, and (if you want functional tests) functionalSuites.
define([
    "node_modules/intern/node_modules/dojo/lang",
    "./base"
], function (lang, base) {

    var config = {


        // Non-functional test suite(s) to run in each browser
        suites: [ "tests/all" ],

        // Functional test suite(s) to run in each browser once non-functional tests are completed
        functionalSuites: [ /* "myPackage/tests/functional" */ ],

        // A regular expression matching URLs to files that should not be included in code coverage analysis
        excludeInstrumentation: /^(?:dojo|buildTmp|node_modules|tests|libs|qbo\/nls|dojo-patch)\//
    };

    config = lang.mixin(base, config);
    return config;
});