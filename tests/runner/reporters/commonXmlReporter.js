"use strict";
/* jshint node: true */

var logger = require("../utils/consoleLogger"),
    utils = require("../utils/utils"),
    fs = require("fs"),
    writeTestsReport = null,
    xmlReporter = null;

writeTestsReport = function (testReport) {
    var reportsDir = "tests/runner/reports",
        xmlFilePath = reportsDir + "/" + "TEST-Intern-report.xml";
    logger.writeln("Generating test suite file...");
    fs.writeFile(xmlFilePath, testReport, function (err) {
        if (err) {
            throw err;
        }
        logger.writeln("Test suite file is generated.");
        logger.writeln("");
    });
    
};

xmlReporter = {

    xmlFile : null,
    suiteName : "main",

    "/runner/start": function () {
        var xmlLine, testSuite;
        xmlLine = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
        testSuite = "<testsuite name='" + "main" + "'  errors='0' failures='0' tests='0' time='0.0020'>";
        this.xmlFile = xmlLine + testSuite;
    },

    "/runner/end": function () {
        this.xmlFile = this.xmlFile + "</testsuite>";
        writeTestsReport(this.xmlFile);
    },

    "/suite/start": function (suite) {
        this.suiteName = suite.name;
    },

    "/test/pass": function (test) {
        var passLine;
        passLine = "<testcase classname=" + "'" + this.suiteName + "'" + " name= \"" + test.name + "\" time= \"" + test.timeElapsed / 1000 + "ms\" />" + "\n";
        this.xmlFile = this.xmlFile + passLine;
    },

    "/test/fail": function (test) {
        var testLine, failLine, endTestLine, failTag, errorMessage;
        errorMessage = utils.getErrorMessage(test.error);
        testLine = "<testcase classname=" + "'" + this.suiteName + "'" + " name= \"" + test.name + "\" time= \"" + test.timeElapsed / 1000 + "ms\" >" + "\n";
        failLine = "<failure type=\"failed\" message= \"" + test.name + " test failed \"><![CDATA[" + errorMessage + "]]></failure>";
        endTestLine = "</testcase>";
        failTag = testLine + "\n" + failLine + "\n" + endTestLine + "\n";
        this.xmlFile = this.xmlFile + failTag;
    }
};

// Export the coverage reported defined above.
for (var property in xmlReporter) {
    if (xmlReporter.hasOwnProperty(property)) {
        exports[property] = xmlReporter[property];
    }
}