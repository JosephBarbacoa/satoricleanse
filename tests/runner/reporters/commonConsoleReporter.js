"use strict";
/* jshint node: true */

var logger = require("../utils/consoleLogger"),
	utils = require("../utils/utils"),
	reporter;

reporter = {

	"/suite/start": function (suite) {
		if (utils.isMainSuite(suite)) {
			return;
		}
		logger.writeln("RUNNING SUITE:" + suite.name);
	},

	"/suite/end": function (suite) {
		var numTests = suite.numTests,
			numFailedTests = suite.numFailedTests,
			logFunc = numFailedTests ? logger.error : logger.success;

		if (utils.isMainSuite(suite)) {
			logger.writeln("Summary:");
			logFunc(numTests - numFailedTests + "/" + numTests + " tests passed");
		} else {
			logFunc(numTests - numFailedTests + "/" + numTests + " tests passed");
			logger.writeln("");
		}
	},

	"/suite/error": function (suite) {
		logger.writeln("ERROR IN SUITE:" + suite.name);
	},

	"/test/pass": function (test) {
		var str = "PASS:" + test.name + " (" + test.timeElapsed + "ms)";
		logger.success(str);
	},

	"/test/fail": function (test) {
		logger.error("FAIL:" + test.name + " (" + test.timeElapsed + "ms)");
		logger.error(utils.getErrorMessage(test.error));
	}
};

for (var property in reporter) {
	if (reporter.hasOwnProperty(property)) {
		exports[property] = reporter[property];
	}
}