"use strict";
/* jshint node: true */

/**
 * Returns an error message extracted from the given error object.
 * @param  {Object} error an object representing the error that occurred. This object should have message or stack fields.
 * @return {String} an error message extracted from the given error object
 */
exports.getErrorMessage = function (error) {
	var message = "";
	if (error.message || error.stack) {
		message = "Error: " + (error.message || "Unknown error") + "\n";

		if (error.stack) {
			message +=
			// V8 puts the original error at the top of the stack too; avoid redundant output that may cause confusion
			// about how many times an assertion was actually called
			error.stack.slice(0, error.message.length + 1) === error.message + "\n" ?
				error.stack.slice(error.message.length) :
				error.stack.slice(0, error.message.length + 8) === "Error: " + error.message + "\n" ?
				error.stack.slice(error.message.length + 8) :
				error.stack;
		} else {
			message += "No stack";
		}

	} else {
		message = error.red;
	}

	return message;
};

/**
 * Returns true if the passed in suite is the main suite for the Intern test run, false otherwise.
 * The Intern framework creates a main suite which wraps around the passed in suites to be run.
 * @param  {Object} suite The suite object to test
 * @return {Boolean} true if the passed in suite is the main sutie for the Intern test run, false otherwise
 */
exports.isMainSuite = function (suite) {
	return suite.name === "main";
};

/**
 * Utility method that uses a provider instrumenter and grunt instance to
 * provide an array of empty istanbul code coverage objects that can be used
 * to expand the scope of code coverage reporting to a file pattern match
 * instead of only what was traced via execution
 */
exports.getEmptyCoverage = function (grunt, instrumenter) {
    var coveragePatterns = [],
        coverage = {},
        filepath = "",
        code = "";

    //massage patterns from uri style format to flat array format
    coveragePatterns = grunt.cli.tasks.filter(function (pattern) {
        var pat = pattern.split("coveragePatterns=")[1];
        return (pat ? true : false);
    }).map(function (pattern) {
        return pattern.split("coveragePatterns=")[1];
    });

    if (coveragePatterns) {
        //do code analysis on each file and manually add coverage object
        grunt.file.expand(coveragePatterns).forEach(function (f) {
            filepath = process.cwd() + "/" + f;
            code = require("fs").readFileSync(filepath, "utf8");
            instrumenter.instrumentSync(code, filepath);
            coverage[filepath] = instrumenter.lastFileCoverage();
        });
    }
    return coverage;
};


