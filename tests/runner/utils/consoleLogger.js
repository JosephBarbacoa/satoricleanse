"use strict";
/* jshint node: true */

require("colors");

exports.error = function (msg) {
    console.log(msg.red);
};

exports.success = function (msg) {
    console.log(msg.green);
};

exports.writeln = function (msg) {
    console.log(msg);
};