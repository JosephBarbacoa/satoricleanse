define([
	"dojo/dom-construct"
], function (domConstruct) {

	var addResultToPage, getErrorText;

	getErrorText = function (error) {
		if (error.message || error.stack) {
			var message = "Error: " + (error.message || "Unknown error") + "\n";

			if (error.stack) {
				message +=
				// V8 puts the original error at the top of the stack too; avoid redundant output that may cause confusion
				// about how many times an assertion was actually called
				error.stack.slice(0, error.message.length + 1) === error.message + "\n" ?
					error.stack.slice(error.message.length) :
					error.stack.slice(0, error.message.length + 8) === "Error: " + error.message + "\n" ?
					error.stack.slice(error.message.length + 8) :
					error.stack;
			} else {
				message += "No stack";
			}
			return message;
		} else {
			return error;
		}
	};

	addResultToPage = function (resultText, fail) {
		var color = fail ? "red" : "green";
		domConstruct.create("div", {
			innerHTML: resultText,
			style: {
				color: color
			}
		}, document.body);
	};

	return {

		"/suite/start": function (suite) {
			if (suite.name === "main") {
				return;
			} else {
				domConstruct.create("div", {
					innerHTML: suite.name,
					style: {
						"font-weight": "bold"
					}
				}, document.body);
			}
		},

		"/suite/end": function (suite) {
			var numTests = suite.numTests,
				numFailedTests = suite.numFailedTests,
				resultText;
			resultText = (numTests - numFailedTests + "/" + numTests + " tests passed");
			addResultToPage(resultText, !!numFailedTests);
			if (suite.name === "main") {
				return;
			} else {
				domConstruct.create("br", null, document.body);
			}
		},

		"/suite/error": function (suite) {
			var message = "SUITE ERROR: in " + suite.name,
				errorDetail = getErrorText(suite.error);
			addResultToPage(message, true);
			addResultToPage(errorDetail, true);
		},

		"/test/pass": function (test) {
			var resultText = "PASS: " + test.name + " (" + test.timeElapsed + "ms)";
			addResultToPage(resultText);
		},

		"/test/fail": function (test) {
			var resultText = "FAIL: " + test.name + " (" + test.timeElapsed + "ms)",
				errorText = getErrorText(test.error);
			addResultToPage(resultText, true);
			addResultToPage(errorText, true);
		}
	};
});