/*
	Wraps the xml reporter node module in an amd module so it can be used with intern.
 */
define([
	"intern/node_modules/dojo/node!../../reporters/commonXmlReporter"
], function (xmlReporter) {
	return xmlReporter;
});