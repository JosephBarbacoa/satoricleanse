define([
	"intern/node_modules/dojo/node!../../../../node_modules/intern/node_modules/istanbul/lib/collector",
	"intern/node_modules/dojo/node!../../../../node_modules/intern/node_modules/istanbul/lib/report/lcovonly"
], function (Collector, Reporter) {
	var collector = new Collector(),
		reporter = new Reporter({dir: "tests/runner/reports"});

	return {
		"/coverage": function (sessionId, coverage) {
			/* jshint unused: false */
			collector.add(coverage);
		},

		"/runner/end": function () {
			reporter.writeReport(collector, true);
		}
	};
});