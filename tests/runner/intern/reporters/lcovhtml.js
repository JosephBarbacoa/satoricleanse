define([
    "intern/node_modules/dojo/node!../../../../node_modules/intern/node_modules/istanbul/lib/collector",
    "intern/node_modules/dojo/node!../../../../node_modules/intern/node_modules/istanbul/lib/report/html",
    "intern/node_modules/dojo/node!../../../../node_modules/intern/node_modules/istanbul/lib/instrumenter",
    "intern/node_modules/dojo/node!../../utils/utils",
    "intern/node_modules/dojo/node!grunt",
    "intern/node_modules/dojo/node!../../../../node_modules/intern/node_modules/istanbul/index"
], function (Collector, Reporter, Instrumenter, utils, grunt) {

    var collector = new Collector(),
        reporter = new Reporter({dir: "tests/runner/reports/html-report"}),
        instrumenter = new Instrumenter();

    //add in empty coverage reporting if applicable
    collector.add(utils.getEmptyCoverage(grunt, instrumenter));

    return {
        "/coverage": function (sessionId, coverage) {
            /* jshint unused: false */
            collector.add(coverage);
        },

        stop: function () {
            reporter.writeReport(collector, true);
        }
    };
});
