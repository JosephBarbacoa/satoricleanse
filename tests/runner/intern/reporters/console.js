/*
	Wraps the console reporter node module in an amd module so it can be used with intern.
 */
define([
	"intern/node_modules/dojo/node!../../reporters/commonConsoleReporter"
], function (consoleReporter) {
	return consoleReporter;
});