define([], function () {

	var logTestEvent, coverageEnabled;

	logTestEvent = function (event) {
		if (typeof window.callPhantom === "function") {
			window.callPhantom(event);
		} else {
			console.error("Phantom reporter being used in non phantom environment");
			console.dir(event);
		}
	};

	coverageEnabled = typeof window.__internCoverage !== "undefined";

	return {

		"/suite/start": function (suite) {
			logTestEvent({
				topic: "/suite/start",
				data: suite.toJSON()
			});
		},

		"/suite/end": function (suite) {
			if (suite.name === "main" && coverageEnabled) {
				logTestEvent({
					topic: "/coverage",
					data: window.__internCoverage
				});
			}
			logTestEvent({
				topic: "/suite/end",
				data: suite.toJSON()
			});

		},

		"/suite/error": function (suite) {
			logTestEvent({
				topic: "/suite/error",
				data: suite.toJSON()
			});
		},

		"/test/pass": function (test) {
			logTestEvent({
				topic: "/test/pass",
				data: test.toJSON()
			});
		},

		"/test/fail": function (test) {
			logTestEvent({
				topic: "/test/fail",
				data: test.toJSON()
			});
		}
	};

});