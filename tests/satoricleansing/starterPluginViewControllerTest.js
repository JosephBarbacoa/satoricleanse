define([
    "intern!object",
    "intern/chai!assert",
    "satoricleansing/StarterPluginViewController"
], function (registerSuite, assert, StarterPluginViewController) {
    var vc;

    registerSuite({

        name: "StarterPluginViewController Tests",

        beforeEach: function () {
            vc = new StarterPluginViewController();
        },

        afterEach: function () {
            vc.destroy();
            vc = null;
        },

        "Test getMessage": function () {
            assert.ok(vc.getMessage());
        }

    });
});
