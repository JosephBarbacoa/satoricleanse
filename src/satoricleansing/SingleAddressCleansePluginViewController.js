define([
    "dojo/_base/declare",
    "neo/BaseViewController",
    "neo/widgets/DialogViewController",
    "dijit/Tooltip",
	"jquery/jquery.min",

    // Template, NLS, CSS
    "text!./templates/StarterPluginView.html"
], function (declare, BaseViewController, DialogController, DijitTooltip) {
    var $ = window.$;

    var TermsDialog = declare(DialogController, {
        dialogContentTemplate:
            "<div>" +
                "<div data-dojo-attach-point='mountElement' style='overflow: auto; height: 200px; width: 400px;'></div>" +
            "</div>"+
            "<div class='buttons'>" +
                "<div>" +
                    "<button type='button' data-dojo-attach-event='onclick:onNo'>No</button>" +
                "</div>" +
                "<div>" +
                    "<button type='button' data-dojo-attach-event='onclick:onYes' class='primary'>Yes</button>" +
                "</div>" +
            "</div>",
        title: "Infuse Contact Correct - Terms of Use",
        //complex: true, // auto sizes the dialog to fit the screen (according to QBO auto sizing conventions)

        onNo: function () {
            this.emit("dialog-fail");
            this.hide();
        },
        onYes: function() {
            this.emit("dialog-success");
            this.hide();
        },

        show: function (newtext) {
            newtext = newtext.replace(/\r\n/g, "<br/>");
            $(this.mountElement).html(newtext);

            return this.inherited(arguments);
        },
    });

    return declare(BaseViewController, {
        templateString: "<div><link rel='stylesheet' href='//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'></div>",

        addressMapObject: null,

        QboUserId: "",

        InstanceId: "",
        ProductInfo: {
            Version: "0.1",
            ProductName: "Infuse Contact Correct",
        },

        buildHttpRequest: function(method, urlExtension, requestData, extraConfigValues) {
            if (!method) {
                method = "GET";
            } else {
                method = method.toUpperCase();
            }
            if (!urlExtension) {
                urlExtension = "";
            }
            if (!requestData) {
                requestData = null;
            }

            var requestConfig = {
                method: method,
                url: "https://ods-sandbox.satorisoftware.com/api/" + urlExtension,
                headers: {
                    "X-Exposure": "G6H4sIAAAAAAAEAKvm5VJQUHJJLU4uyiwoyczPU7JSUHJKLM5MVgh08ldIrSjILy4tSlUoyc9OzVPSAasOKMpPKU0uCc5Mz0ssAUqCtPi6O0YGGBu6mnq5mId5hPqHhwZ4+pgFeoQp8XLVAgDQoI+7ZQAAAA=="
                },
                data: requestData
            };

            for (var attrname in extraConfigValues) {
                if (typeof (extraConfigValues[attrname]) === "object") {
                    for (var objectAttrname in extraConfigValues[attrname]) {
                        if (extraConfigValues[attrname].hasOwnProperty(objectAttrname)) {
                            requestConfig[attrname][objectAttrname] = extraConfigValues[attrname][objectAttrname];
                        }
                    }
                } else {
                    requestConfig[attrname] = extraConfigValues[attrname];
                }
            }
            return requestConfig;
        },
        sendDseEvent: function (action, actionDetails, userId) {
            var that = this;
            var dataObject = {};
            dataObject.InstanceId = that.InstanceId;
            dataObject.ProductInfo = that.ProductInfo;
            dataObject.Action = action;
            // if null or not an object
            if (!actionDetails || typeof (actionDetails) !== "object") {
                actionDetails = {};
            }
            dataObject.Details = actionDetails;

            var headerobject = {
                contentType: "application/json",
                headers: {
                    "X-Exposure": "G6H4sIAAAAAAAEAKvm5VJQUHJJLU4uyiwoyczPU7JSUHJKLM5MVgh08ldIrSjILy4tSlUoyc9OzVPSAasOKMpPKU0uCc5Mz0ssAUqCtPi6O0YGGBu6mnq5mId5hPqHhwZ4+pgFeoQp8XLVAgDQoI+7ZQAAAA==",
                    Authorization: "token " + btoa(userId)
                },
            };
            var reg = that.buildHttpRequest("POST", "events/QboMetrics", JSON.stringify(dataObject), headerobject);
            $.ajax(reg).error(function() {
                console.log("Error sending event to DSE.");
            });

            $("button#satoriCleanseCustomerBtn", that.address_map).removeAttr("disabled");
        },
        showUserErrorAndSendDseEvent: function(qboErrorText, dseAction, dseErrorText, eventData, userId) {
            var that = this;
            // log that the user refused the terms of service
            console.log(dseErrorText);
            if (!eventData || typeof (eventData) !== "object") { // if null or not an object
                eventData = {};
            }
            that.sendDseEvent(dseAction, eventData, userId);

            // show error icon
            if ($("span#satoriCleanseTooltip").size() > 0) {
                $("span#satoriCleanseTooltip").remove();
            }
            var iconsParent = $(that.addressMapObject).first().parent();
            var tooltipSpan = ($("<span>", {
                id: "satoriCleanseTooltip",
                style: " float: right; width: 23px; height: 21px; " +
                    "background-size: 40px; background-position: 0 0;", // background-position 0,0 for red "!" (0,-40 for checkmark)
                "class": "icon secondary-color-sprite confirm-small"
            }));
            iconsParent.append(tooltipSpan);
            new DijitTooltip({
                connectId: ["satoriCleanseTooltip"],
                label: qboErrorText,
                position: ["above", "below"],
                showDelay: 0
            });
        },

        checkTermsForUser: function (userId) {
            //debugger;
            var that = this;

            // disable btn to avoid more than one click event
            $("button#satoriCleanseCustomerBtn", that.address_map).attr("disabled", "disabled");
            
            // if user has accepted terms, cleanse the address
            // else assume that they don't have an account
            if (!userId || typeof (userId) === "object") { userId = qbo.user.id; }
//            if (!userId || typeof (userId) === "object") { userId = "joes91"; }
            var headerobject = {
                headers: {
                    "X-Exposure": "G6H4sIAAAAAAAEAKvm5VJQUHJJLU4uyiwoyczPU7JSUHJKLM5MVgh08ldIrSjILy4tSlUoyc9OzVPSAasOKMpPKU0uCc5Mz0ssAUqCtPi6O0YGGBu6mnq5mId5hPqHhwZ4+pgFeoQp8XLVAgDQoI+7ZQAAAA==",
                    Authorization: "token " + btoa(userId)
                },
            };
            var reg = that.buildHttpRequest("GET", "accounts/" + userId + "/terms", null, headerobject);
            $.ajax(reg)
                .success(function(data) {
                    if (data[0].Accepted) {
                        that.cleanseAddress(userId);
                    } else {
                        that.acceptDseTerms(userId); // accepts terms
                    }
                })
                .error(function() {
                    console.log("Error getting terms for account. Assuming account not created. Creating.");
                    that.createAccountForUser(userId); // creates and accepts terms
                });
        },
        createAccountForUser: function (userId) { // creates and accepts terms
            //debugger;
            var that = this;
            var postObject = {
                "UserName": userId,
                "Password": userId,
                "ProductKey": "",
                "SalesLogixID": userId,
                "FirstName": qbo.user.firstName,
                "LastName": qbo.user.lastName,
                "EmailAddress": qbo.user.email,
                "Organization": qbo.companyName,
                "AddressLine1": "",
                "AddressLine2": "",
                "City": "",
                "State": "",
                "PostalCode": "",
                "PhoneNumber": "",
                "FaxNumber": "",
                "ContactPrecedence": 1,

                CustomAuthTokens: [{
                    Name: "QBO Satori Address Cleansing Call",
                    Token: userId,
                    Application: "QBO",
                    Expiration: null,
                }],
            };
            var headerObject = {
                contentType: "application/json",
                headers: {
                    "X-Exposure": "G6H4sIAAAAAAAEAKvm5VJQUHJMTs4vzSsJKMovyyzOzM9LLQrJz07NU7JSUDJzNDAPd/F1cQoI8zUzdvU3NzOO8HA0cnVU0gFrdUktTi7KLCgB6gIph5qkgGSUQmpFQX5xaVGqQgnYUIg+oIKU0uSS4Mz0vMQSoCRIs6+7Y2SAsaGrqZeLeZhHqH94aICnj1mgR5gSL1ctAPJkGyyqAAAA"
                },
            };
            var reg = that.buildHttpRequest("POST", "accounts/", JSON.stringify(postObject), headerObject); // create account
            $.ajax(reg).success(function () {
                that.sendDseEvent("new user account created", {}, that.QboUserId);

                // we assume that (because we are creating an account) the user has not accepted terms
                that.acceptDseTerms(userId); // accepts terms
            }).error(function (error) {
                console.log("Error while trying to create an account for " + userId + " :" + error.statusText +
                    ". Assuming user account is created and giving user terms dialog. ");
                that.acceptDseTerms(userId);
            });
        },
        acceptDseTerms: function (username) {
            //debugger;
            var that = this;

            // first get terms to show to user (we assume that the user has not accepted terms)
            var headerobject = {
                headers: {
                    "X-Exposure": "G6H4sIAAAAAAAEAKvm5VJQUHJJLU4uyiwoyczPU7JSUHJKLM5MVgh08ldIrSjILy4tSlUoyc9OzVPSAasOKMpPKU0uCc5Mz0ssAUqCtPi6O0YGGBu6mnq5mId5hPqHhwZ4+pgFeoQp8XLVAgDQoI+7ZQAAAA==",
                    Authorization: "token " + btoa(username)
                },
            };
            var reg = that.buildHttpRequest("GET", "accounts/" + username + "/terms", null, headerobject);
            $.ajax(reg)
                .success(function (data) {
                    // once we have the terms, show them to the user and allow them to accept/deny them
                    var dialog = new TermsDialog();
                    dialog.startup();
                    that.own(dialog);
                    dialog.show(data[0].Terms);

                    // if the user accepts them, call DSE API to save the acceptance
                    dialog.on("dialog-success", function () {
                        headerobject = {
                            headers: {
                                "X-Exposure": "G6H4sIAAAAAAAEAKvm5VJQUHJJLU4uyiwoyczPU7JSUHJKLM5MVgh08ldIrSjILy4tSlUoyc9OzVPSAasOKMpPKU0uCc5Mz0ssAUqCtPi6O0YGGBu6mnq5mId5hPqHhwZ4+pgFeoQp8XLVAgDQoI+7ZQAAAA==",
                                Authorization: "token " + btoa(username)
                            },
                            contentType: "text/json",
                        };
                        var requestObject = {
                            "TermsName": "Data Services License Agreement",
                            "Accepted": "true"
                        };
                        reg = that.buildHttpRequest("POST", "accounts/" + username + "/terms", JSON.stringify(requestObject), headerobject); // accounts/1347348420/terms/Data%20Services%20License%20Agreement
                        $.ajax(reg).success(function() {
                                that.checkTermsForUser(username);
                            })
                            .error(function(error) {
                                var qboError = "Unable to communicate with server. Please try again soon.";
                                var dseError = "Error while trying to accept terms for a user: " + error.statusText;
                                var dseAction = "Error";
                                var dseObject = {
                                    "Message": dseError
                                };
                                that.showUserErrorAndSendDseEvent(qboError, dseAction, dseError, dseObject, username);
                            });
                    });

                    // if the user refuses the terms, log that refusal in the DSE API
                    dialog.on("dialog-fail", function () {
                        var qboError = "Terms not accepted, address was not processed.";
                        var dseError = "User " + username + " has refused Terms.";
                        var dseAction = "TermsRefusal";
                        var dseObject = {}; // no body for terms refusal
                        that.showUserErrorAndSendDseEvent(qboError, dseAction, dseError, dseObject, username);
                    });
                })
                .error(function (error) {
                    var qboError = "Unable to communicate with server. Please try again soon.";
                    var dseError = "Error getting terms for account " + username + ": " + error.statusText;
                    var dseAction = "Error";
                    var dseObject = {
                        "Message": dseError
                    };
                    that.showUserErrorAndSendDseEvent(qboError, dseAction, dseError, dseObject, username);
                });
        },

        // if there is address data, the data is stored
        storeAddressData: function () {
            var that = this;
            var iconsParent = $(that.addressMapObject).first().parent();
            var inputsParent = iconsParent.parent();

            var addressData = {
                "OutputFieldList": [ // if OutputFieldList gets edited, be sure to update functions: cleanseAddress, undoAddressCleanse
                    "AddressLine1", "AddressLine2", "City", "State", "ZipCode",
                    "ErrorCode", "AddressCorrectionMessage", "CarrierRoute", "DPBarcode", "DpvIndicator", "IsVacant",
                    "Business"],
                "InputFieldList": ["AddressLine1", "City", "State", "ZipCode", "Country", "Business"],
                "Records": [
                    [
                        // NOTE: we can't use the parent model (that.getParent().model) because auto fill features (such as chrome's built in feature) do not fill in those values
                        $("textarea[name=street]", inputsParent).val(), // first address line
                        $("input[name=city]", inputsParent).val(), // city
                        $("input[name=state]", inputsParent).val(), // state
                        $("input[name=postalCode]", inputsParent).val(), // zip
                        $("input[name=country]", inputsParent).val(), // country (not currently processed)
                        $(inputsParent).closest(".tabContainer").siblings().find("input[name=companyName]").val() // business
                    ]
                ]
            };

            // if there is some address data, store it
            if(!!addressData.Records[0][0] || !!addressData.Records[0][1] || !!addressData.Records[0][2] || !!addressData.Records[0][3] || !!addressData.Records[0][4]) {
                var stringifiedAddressRecords = JSON.stringify(addressData.Records[0]);
                var satoriButton = iconsParent.find("button.satoriCleanseBtn");
                if(!satoriButton.attr("data-oldaddress")) { // only add address data if there isn't any there already
                    satoriButton.attr("data-oldaddress", stringifiedAddressRecords);
                }
            }

            return addressData;
        },
        cleanseAddress: function (username) {
            var that = this;
            if (!username) { username = qbo.user.id; }
            var iconsParent = $(that.addressMapObject).first().parent();
            var addressData = that.storeAddressData();
            var headerobject = {
                headers: {
                    "X-Exposure": "G6H4sIAAAAAAAEAKvm5VJQUHJJLU4uyiwoyczPU7JSUHJKLM5MVgh08ldIrSjILy4tSlUoyc9OzVPSAasOKMpPKU0uCc5Mz0ssAUqCtPi6O0YGGBu6mnq5mId5hPqHhwZ4+pgFeoQp8XLVAgDQoI+7ZQAAAA==",
                    Authorization: "token " + btoa(username)
                }
            };
            var reg = that.buildHttpRequest("POST", "SingleAddress/US", addressData, headerobject);
            $.ajax(reg).success(function(result){
                    var model = that.getParent().model;
                    ["street", null, "city", "state", "postalCode"].map(function (attr, i) {
                        if(attr) {
                            model.set("address_" + attr, result.Records[0][i]);
                        }
                    }); // NOTE: country is currently ignored 
                    model.set("companyName", result.Records[0][11]);

                    // create result icon and give it the approriate style and tooltip
                    if($("span#satoriCleanseTooltip").size() > 0) {
                        $("span#satoriCleanseTooltip").remove();
                    }
                    if($("span#satoriCleanseUndoCorrection").size() > 0) {
                        $("span#satoriCleanseUndoCorrection").remove();
                    }

                    // ErrorCode, AddressCorrectionMessage, DpvIndicator, IsVacant
                    var statusInfo = that.deduceStatusInformation(parseInt(result.Records[0][5]),
                        result.Records[0][6], result.Records[0][9], result.Records[0][10]);

                    var correction = "background-position: 0 -40px;"; // good correction
                    // bad correction / error
                    if(statusInfo.status === "error") {
                        correction = "background-position: 0 0;";
                    } // some correction
                    else if(statusInfo.status === "warning") {
                        correction = "background-position: 0 -440px;";
                    }
                    var tooltipSpan = ($("<span>", {
                        id: "satoriCleanseTooltip",
                        //title: statusInfo.message,
                        style: " float: right; width: 23px; height: 21px; " +
                            "background-size: 40px;" + correction, // background-position 0,0 for red "!" (0,-40 for checkmark)
                        "class": "icon secondary-color-sprite confirm-small"
                    }));
                    iconsParent.append(tooltipSpan);
                    new DijitTooltip({
                        connectId: ["satoriCleanseTooltip"],
                        label: statusInfo.message,
                        position: ["above", "below"],
                        showDelay: 0
                    });

                    var undoSpan = ($("<span>", {
                        id: "satoriCleanseUndoCorrection",
                        style: "float: right; font-size: 21px; cursor: pointer; padding: 0px 4px;",
                        "class": "fa fa-undo"
                    }));
                    undoSpan.click(that.undoAddressCleanse.bind(that));
                    iconsParent.append(undoSpan);
                    new DijitTooltip({
                        connectId: ["satoriCleanseUndoCorrection"],
                        label: "Click to revert to original address",
                        position: ["above", "below"],
                        showDelay: 0
                    });

                    var dataObject = {
                        "ZipCode": result.Records[0][4],
                        "ErrorCode": result.Records[0][5]
                    };
                    that.sendDseEvent("CorrectAddress", dataObject, username);
                })
                .error(function(error){
                    var qboError = "There was a server error.";
                    var dseAction = "Error";
                    var dseError = "Error correcting address for " + username + ": " + error.statusText;
                    var dseObject = {
                        "Message": dseError
                    };
                    that.showUserErrorAndSendDseEvent(qboError, dseAction, dseError, dseObject, username);
                });
        },

        undoAddressCleanse: function() {
            var that = this;
            var iconsParent = $(that.addressMapObject).first().parent();
            var satoriButton = iconsParent.find("button.satoriCleanseBtn");
            var addressRecordsObject = $.parseJSON(satoriButton.attr("data-oldaddress"));

            var model = that.getParent().model;
            ["street", "city", "state", "postalCode"].map(function(attr, i) {
              model.set("address_" + attr, addressRecordsObject[i]);
            });
            model.set("companyName", addressRecordsObject[5]); // business 

            // after undoing the address, the tooltip and undo icons dont't hold pertinent information anymore and are removed
            $("span#satoriCleanseUndoCorrection").remove();
            $("span#satoriCleanseTooltip").remove();

            that.sendDseEvent("AddressUndo", {}, qbo.user.id);
        },

        startup: function() {
            var that = this;
            that.inherited(arguments);
            that.QboUserId = qbo.user.id;
            that.InstanceId = (new Date().getTime()).toString();
            that.addressMapObject = that.getParent().address_map;
            //debugger;

            var parentType = that.getParent().type;
            if (parentType === "customer") {
                console.log("good, we are on the customer dialog");
            } else if (parentType === "employee") {
                console.log("good, we are on the employee dialog");
            } else if (parentType === "vendor") {
                console.log("good, we are on the vendor dialog");
            } else {
                console.log("BAD! We we are not on a recognized dialog");
                return;
            }

            that.getParent().on("page-ready", function() {
                console.log("ready");
            });

            var clearIconsAndStoredAddressData = function () {
                // clear icon and data-oldaddress
                $("button.satoriCleanseBtn").removeAttr("data-oldaddress");
                $("span#satoriCleanseTooltip").remove();
                $("span#satoriCleanseUndoCorrection").remove();
            };
            that.getParent().on("dialog-cancel", function(){
                clearIconsAndStoredAddressData();
                that.sendDseEvent("dialog-cancel", {}, that.QboUserId);
            });
            that.getParent().on("dialog-success", function () {
                clearIconsAndStoredAddressData();
                that.sendDseEvent("dialog-save", {}, that.QboUserId);
            });

            // HACK: try to find a better way
            setTimeout(function() {
                var button;

                // TODO: use "that.addressMapObject" and if-elseif-elseif-else{log error} instead of if-if-if
                // check if we are on the "Customer" page
                if ($("div.form.fullAdd.customerForm a.mapLink").size() > 0) {
                    if ($("button#satoriCleanseCustomerBtn").size() === 0) {
                        button = ($("<button>", {
                            id: "satoriCleanseCustomerBtn",
                            text: "Correct Address",
                            type: "button",
                            "class": "primary satoriCleanseBtn",
                            style: "float: right; margin-right: 26px; height: 22px; line-height: 0; padding: 0 10px;",
                        }).click(that.checkTermsForUser.bind(that)));
                        $("div.form.fullAdd.customerForm a.mapLink").first().parent().append(button);
                    }
                }
                // check if we are on the "Vendor" page
                if ($("div.form.fullAdd.vendorForm a.mapLink").size() > 0) {
                    if ($("button#satoriCleanseVendorBtn").size() === 0) {
                        button = ($("<button>", {
                            id: "satoriCleanseVendorBtn",
                            text: "Correct Address",
                            type: "button",
                            "class": "primary satoriCleanseBtn",
                            style: "float: right; margin-right: 26px; height: 22px; line-height: 0;",
                        }).click(that.checkTermsForUser.bind(that)));
                        $("div.form.fullAdd.vendorForm a.mapLink").first().parent().append(button);
                    }
                }
                // check if we are on the "Employee" page
                if ($("div.form.fullAdd.employeeForm a.mapLink").size() > 0) {
                    if ($("button#satoriCleanseEmployeeBtn").size() === 0) {
                        button = ($("<button>", {
                            id: "satoriCleanseEmployeeBtn",
                            text: "Correct Address",
                            type: "button",
                            "class": "primary satoriCleanseBtn",
                            style: "float: right; margin-right: 26px; height: 22px; line-height: 0;",
                        }).click(that.checkTermsForUser.bind(that)));
                        $("div.form.fullAdd.employeeForm a.mapLink").first().parent().append(button);
                    }
                }

                var dialogType = "new ";
                // if this is an Edit dialog (!new) store any prexisting address data
                if (!that.getParent().model.isNew()) {
                    dialogType = "edit ";
                    that.storeAddressData();
                }

                // log dialog opened
                that.sendDseEvent(
                    "dialog-opened",
                    {
                        "parent-type": parentType,
                        "dialog-type": dialogType + parentType
                    },
                    that.QboUserId
                );
            }, 2000);

            that.pageReady();
        },

	    // Given:
	    //      * correction code - code as returned - from address correction
	    //      * correction code msg - the message string - from address correction
	    //      * dvpIndicator- from address correction
	    //      * dpvVacant indicator  - from address correction
	    //
	    // Will deduce the correct message to display to the user and the overall status of the correction:
	    //      error, warning, success -- these go to choosing the status icon
	    //
	    // returns:
	    //   {
	    //      status: [success,error,warning],
	    //      message: [a displayable message]
	    //   }
        deduceStatusInformation: function(cCode, cCodeMsg, dpvIndicator, dpvIsVacant) {
            var overallStatus = {
                success: "success",
                error: "error",
                warning: "warning"
            };

            var concatWithSpace = function(baseMsg, toAdd) {
                return concatWith(baseMsg, toAdd, " ");
            };

            var concatWithBreak = function(baseMsg, toAdd) {
                return concatWith(baseMsg, toAdd, "<br/>");
            };

            var concatWith = function(baseMsg, toAdd, delim) {
                var outMsg = baseMsg;

                if (outMsg === null) {
                    outMsg = "";
                }

                if (toAdd === null || toAdd.length === 0) {
                    return outMsg;
                }

                if (outMsg.length > 0 && !(new RegExp(delim + "$")).test(outMsg)) {
                    outMsg += delim;
                }

                outMsg += toAdd;
                return outMsg;
            };

            var statusMsgs = {
                rangeNoChanges: "The address is correct.",

                rangeSpecial: "The address matched to an alias address.",
                rangeLacsLinkAdjusted: "The address is correct, but was updated to a more standard version of the address.",
                rangeSuiteLinkAdjusted: "Suite information was added for this business",
                rangePrimaryLineAdjusted: "Street information was updated.",

                rangeCityUpdated: "City updated.",
                rangeStateUpdated: "State updated.",
                rangeZipUpdated: "Zip code updated.",

                rangeCityStateZipUpdated: "The City, State and Zip were updated.",
                rangeCityStateUpdated: "The City and State were updated.",
                rangeCityZipUpdated: "The City and Zip were updated.",
                rangeStateZipUpdated: "The State and Zip were updated.",

                isVacant: "The address is currently vacant.",
                isNotVacant: "The address is not vacant.",
                secondaryMissing: "Unit information expected, but missing.",
                secondaryInvalid: "Unit information is present, but not correct.",

                isDeliverable: "The address is deliverable.",
                isNotDeliverable: "The address is not deliverable, mail will be returned.",
            };

            // 01-10 No changes to input address
            var rangeNoChanges = [0, 7, 8, 91, 92, 93, 94];

            // 11-20 Special situations
            var rangeSpecial = [11, 12, 13];
            var rangeLacsLinkAdjusted = [9];
            var rangeSuiteLinkAdjusted = [10];
            // 31-99 Delivery Address Line Changes
            var rangePrimaryLineAdjusted = [31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86];
            //var rangeCityUpdated = [21,22,23,24,35,36,37,38,42,43,44,45,49,50,51,52,56,57,58,59,65,66,67,68,72,73,74,75,80,81,82,83];
            //var rangeStateUpdated = [22,24,26,27,36,38,40,41,43,45,47,48,50,52,54,55,57,59,61,62,66,68,70,71,73,77,78,81,85,86];
            //var rangeZipUpdated = [23,24,25,26,37,38,39,40,44,45,46,47,51,52,53,54,58,59,60,61,67,68,69,70,74,75,76,77,82,83,84,85];
            //var rangeDPVWarning = [91, 92, 93, 94];

            var rangeCityUpdated = [21];
            var rangeStateUpdated = [27];
            var rangeZipUpdated = [25];

            var rangeCityStateZipUpdated = [24, 38, 45, 52, 59, 68, 75, 83];
            var rangeCityStateUpdated = [22, 36, 43, 50, 57, 66, 73, 81];
            var rangeCityZipUpdated = [23, 37, 44, 51, 58, 67, 74, 82];
            var rangeStateZipUpdated = [26, 40, 47, 54, 61, 70, 77];

            // Uncorrected
            var rangeUncorrected = [111, 112, 113, 114, 211, 212, 213, 214, 215, 216, 311, 312, 313, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 491, 492, 493, 494, 495];

            var msgBin = [{
                msg: statusMsgs.rangeNoChanges,
                range: rangeNoChanges,
                status: overallStatus.success
            }, {
                msg: statusMsgs.rangeNoChanges, // Use the 'correct' message instead of some alias verbage.
                range: rangeSpecial,
                status: overallStatus.success
            }, {
                msg: statusMsgs.rangeLacsLinkAdjusted,
                range: rangeLacsLinkAdjusted,
                status: overallStatus.warning
            }, {
                msg: statusMsgs.rangeSuiteLinkAdjusted,
                range: rangeSuiteLinkAdjusted,
                status: overallStatus.warning
            }, {
                msg: statusMsgs.rangePrimaryLineAdjusted,
                range: rangePrimaryLineAdjusted,
                status: overallStatus.warning
            }, {
                msg: statusMsgs.rangeCityUpdated,
                range: rangeCityUpdated,
                status: overallStatus.warning
            }, {
                msg: statusMsgs.rangeStateUpdated,
                range: rangeStateUpdated,
                status: overallStatus.warning
            }, {
                msg: statusMsgs.rangeZipUpdated,
                range: rangeZipUpdated,
                status: overallStatus.warning
            }, {
                msg: statusMsgs.rangeCityStateZipUpdated,
                range: rangeCityStateZipUpdated,
                status: overallStatus.warning
            }, {
                msg: statusMsgs.rangeCityStateUpdated,
                range: rangeCityStateUpdated,
                status: overallStatus.warning
            }, {
                msg: statusMsgs.rangeCityZipUpdated,
                range: rangeCityZipUpdated,
                status: overallStatus.warning
            }, {
                msg: statusMsgs.rangeStateZipUpdated,
                range: rangeStateZipUpdated,
                status: overallStatus.warning
            }, {
                msg: null,
                range: rangeUncorrected,
                status: overallStatus.error
            }];


            var result = {
                status: overallStatus.warning,
                message: ""
            };

            // Build the primary correction message.
            for (var iter in msgBin) {
                if (msgBin[iter].range.indexOf(cCode) >= 0) {
                    var tmpMsg = msgBin[iter].msg;

                    if (tmpMsg === null) {
                        tmpMsg = cCodeMsg;
                    }

                    result.message = concatWithSpace(result.message, tmpMsg);
                    result.status = msgBin[iter].status;
                }
            }

            // Indicators - S/D (secondary information missing or incorrect)
            if (dpvIndicator !== null && !!dpvIndicator.trim()) { // if not null or empty/whitespace
                switch (dpvIndicator) {
                    case "Y":
                        // Vacancy (only add vacancy if there is a "Y" dpvIndicator)
                        if(dpvIsVacant !== null) {
                            switch(dpvIsVacant) {
                                case "1":
                                {
                                    result.status = overallStatus.warning;
                                    result.message = concatWithBreak(result.message, statusMsgs.isVacant);
                                    break;
                                }
                                case "0":
                                {
                                    result.message = concatWithBreak(result.message, statusMsgs.isNotVacant);
                                    break;
                                }
                            }
                        }
                        result.message = concatWithBreak(result.message, statusMsgs.isDeliverable);

                        break;
                    case "N": result.message = concatWithBreak(result.message, statusMsgs.isNotDeliverable); break;
                    case "S": result.message = concatWithBreak(result.message, statusMsgs.secondaryInvalid); break;
                    case "D": result.message = concatWithBreak(result.message, statusMsgs.secondaryMissing);
                        result.status = overallStatus.warning;
                        break;
                    default:
                        var dpvMessage = "HIT DEFAULT ON dpvIndicator, unrecognized DPV Indicator type '" + dpvIndicator + "'";
                        console.log(dpvMessage);
                        var detailsObject = { "DPV-Indicator-Message": dpvMessage };
                        this.sendDseEvent("CorrectAddress", detailsObject, qbo.user.id);
                        break;
                }

            }

            return result;
        },
    });
});
